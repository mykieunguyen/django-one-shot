from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoItem, TodoList
from todos.forms import TodoForm, TodoItemForm

# Create your views here.

# Homepage: List all to do lists


def todos_list(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos
    }

    return render(request, "todos/list.html", context)

# Detail page: Show items of a to-do list


def show_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo
    }

    return render(request, "todos/detail.html", context)

# Create new todo list from homepage


def create_todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


# Homepage: Create new Item
def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create_item.html", context)

# Detail page: edit todo list name


def update_todo_name(request, id):
    todo_list = TodoList.objects.get(id=id)

    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoForm(instance=todo_list)

    context = {
        "form": form,
        "todo_list_object": todo_list
    }

    return render(request, "todos/update_list.html", context)

# Detail Page: Edit List Item


def update_todo_item(request, id):
    item = TodoItem.objects.get(id=id)

    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        "form": form,
        'todo_list_item': item
    }

    return render(request, "todos/update_item.html", context)

# Detail Page: Delete To Do list


def delete_todo_list(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete_list.html")
